package com.dronedelivery.score;

import com.dronedelivery.domain.Delivery;
import com.dronedelivery.domain.Order;
import com.dronedelivery.domain.ScheduleResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.dronedelivery.DroneDelivery.DRONE_LAUNCH_TIME;
import static com.dronedelivery.DroneDelivery.DRONE_RETURN_TIME;
import static com.dronedelivery.score.Scores.NPS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link NetPromoterScore}.
 */
@ExtendWith(MockitoExtension.class)
class NetPromoterScoreTest {
  private NetPromoterScore nps;

  @Mock
  private Order order1;

  @Mock
  private Order order2;

  private List<Order> orders;

  @BeforeEach
  void resetOrders() {
    nps = new NetPromoterScore();
    orders = new ArrayList<>();
  }

  /**
   * Mocks an order and adds it to the list of orders.
   *
   * @param order the order to mock
   * @param receivedAt when the order was received
   * @param deliveryTime the delivery time of the order
   */
  private void mockOrder(Order order, LocalTime receivedAt, Duration deliveryTime) {
    orders.add(order);
    when(order.getReceivedAt()).thenReturn(receivedAt);
    when(order.getRoundTripTime()).thenReturn(deliveryTime.multipliedBy(2));
    when(order.getDeliveryTime()).thenReturn(deliveryTime);
  }

  @Test
  void schedule() {
    mockOrder(order1, LocalTime.of(5, 0, 0), Duration.ofHours(1));
    mockOrder(order2, LocalTime.of(5, 30, 0), Duration.ofMinutes(15));

    ScheduleResult schedule = nps.schedule(orders, DRONE_LAUNCH_TIME, DRONE_RETURN_TIME);
    assertThat(schedule.getScoringMethod()).isEqualTo(NPS);

    List<Delivery> deliveries = schedule.getScheduledDeliveries();
    assertThat(deliveries.size()).isEqualTo(2);

    Delivery delivery = deliveries.get(0);
    assertThat(delivery.getOrder()).isEqualTo(order2);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(DRONE_LAUNCH_TIME));
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofMinutes(45));

    delivery = deliveries.get(1);
    assertThat(delivery.getOrder()).isEqualTo(order1);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(DRONE_LAUNCH_TIME.plusMinutes(30)));
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofMinutes(150));
  }

  @Test
  void scheduleUndeliverable() {
    mockOrder(order1, LocalTime.of(20, 0, 1), Duration.ofHours(1));
    mockOrder(order2, LocalTime.of(5, 30, 0), Duration.ofMinutes(15));

    ScheduleResult schedule = nps.schedule(orders, DRONE_LAUNCH_TIME, DRONE_RETURN_TIME);
    assertThat(schedule.getScoringMethod()).isEqualTo(NPS);

    List<Delivery> deliveries = schedule.getScheduledDeliveries();
    assertThat(deliveries.size()).isEqualTo(2);

    Delivery delivery = deliveries.get(0);
    assertThat(delivery.getOrder()).isEqualTo(order2);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(DRONE_LAUNCH_TIME));
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofMinutes(45));

    delivery = deliveries.get(1);
    assertThat(delivery.getOrder()).isEqualTo(order1);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.empty());
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofDays(1));
  }

  @Test
  void scheduleDelayed() {
    mockOrder(order1, LocalTime.of(20, 0, 0), Duration.ofMinutes(2));
    mockOrder(order2, LocalTime.of(20, 30, 0), Duration.ofMinutes(2));

    ScheduleResult schedule = nps.schedule(orders, DRONE_LAUNCH_TIME, DRONE_RETURN_TIME);
    assertThat(schedule.getScoringMethod()).isEqualTo(NPS);

    List<Delivery> deliveries = schedule.getScheduledDeliveries();
    assertThat(deliveries.size()).isEqualTo(2);

    Delivery delivery = deliveries.get(0);
    assertThat(delivery.getOrder()).isEqualTo(order1);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(LocalTime.of(20, 0, 0)));
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofMinutes(2));

    delivery = deliveries.get(1);
    assertThat(delivery.getOrder()).isEqualTo(order2);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(LocalTime.of(20, 30, 0)));
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofMinutes(2));
  }
}
