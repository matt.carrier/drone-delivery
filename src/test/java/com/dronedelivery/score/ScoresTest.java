package com.dronedelivery.score;

import org.junit.jupiter.api.Test;

import static com.dronedelivery.score.Scores.NPS;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link Scores}.
 */
class ScoresTest {
  @Test
  void createNPS() {
    assertThat(NPS.create()).isInstanceOf(NetPromoterScore.class);
  }
}
