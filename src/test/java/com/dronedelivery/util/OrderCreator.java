package com.dronedelivery.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.dronedelivery.io.FileBasedOrderIntake.TIME_FORMATTER;

/**
 * Creates order files for testing.
 */
public class OrderCreator {
  private static final Logger log = LogManager.getLogger(OrderCreator.class);

  private static final int NUM_ORDERS = 100;
  private static final int TOWN_SIZE = 50;
  private static final int VECTOR_MAGNITUDE_MAX = TOWN_SIZE / 2;

  /**
   * Creates a test file.
   *
   * @throws IOException if there was an issue creating the file
   */
  private void createOrderFile() throws IOException {
    final File file = File.createTempFile("drone-delivery-orders", ".in");

    try (final FileWriter writer = new FileWriter(file)) {
      // create a sorted list of order receivedAts
      List<LocalTime> orderReceivedAtList = IntStream.range(0, NUM_ORDERS).boxed()
        .map(i -> LocalTime.ofSecondOfDay((int) (Math.random() * Duration.ofDays(1).getSeconds())))
        .sorted()
        .collect(Collectors.toList());

      // create an order for each receivedAt
      IntStream.range(0, NUM_ORDERS).boxed().forEach(i -> {
        final String orderId = String.format("WM%04d", i);

        final int nsVectorMag = (int) (Math.random() * VECTOR_MAGNITUDE_MAX);
        final int ewVectorMag = (int) (Math.random() * VECTOR_MAGNITUDE_MAX);
        final String nsVector = (Math.random() < 0.5 ? "N" : "S") + nsVectorMag;
        final String ewVector = (Math.random() < 0.5 ? "E" : "W") + ewVectorMag;
        final String coords = nsVector + ewVector;
        try {
          writer.write(String.format("%s %s %s\n", orderId, coords, orderReceivedAtList.get(i).format(TIME_FORMATTER)));
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
    }

    log.info(String.format("File [%s] created with [%d] records", file.getAbsolutePath(), NUM_ORDERS));
  }

  public static void main(String[] args) throws IOException {
    new OrderCreator().createOrderFile();
  }
}
