package com.dronedelivery;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dronedelivery.io.FileBasedOrderIntake.TIME_FORMATTER;
import static com.dronedelivery.score.Scores.NPS;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Holistic integration tests for {@link DroneDelivery}.
 */
@Integration
class DroneDeliveryTest {
  private DroneDelivery delivery;

  @BeforeEach
  void createDroneDeliveryAndInputFile() {
    delivery = new DroneDelivery();
    delivery.file = new File(DroneDeliveryTest.class.getResource("/example_orders.in").getPath());
  }

  @Test
  void verifyFile() throws Exception {
    final List<String> lines;
    try (final FileReader fileReader = new FileReader(delivery.call());
         final BufferedReader reader = new BufferedReader(fileReader)) {
      lines = reader.lines().collect(Collectors.toList());
    }

    assertThat(lines.size()).isEqualTo(5);
    final Set<String> orderIds = lines.stream()
      .filter(l -> !l.startsWith(NPS.name()))
      .map(l -> l.substring(0, l.indexOf(" ")))
      .collect(Collectors.toSet());

    assertThat(orderIds).containsExactlyInAnyOrder("WM001", "WM002", "WM003", "WM004");
    lines.forEach(l -> assertThat(l.split(" ").length).isEqualTo(2));
    lines.stream()
      .filter(l -> !l.startsWith(NPS.name()))
      .map(l -> l.substring(l.indexOf(" ") + 1))
      .forEach(t -> assertThat(LocalTime.parse(t, TIME_FORMATTER)).isNotNull());

    String scoreLine = lines.get(4);
    assertThat(scoreLine.startsWith(NPS.name()));

    int score = Integer.valueOf(scoreLine.substring(scoreLine.indexOf(" ") + 1));
    assertThat(score).isGreaterThan(-1);
    assertThat(score).isLessThan(101);
  }
}
