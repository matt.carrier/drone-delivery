package com.dronedelivery.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.NANOS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link Delivery}.
 */
@ExtendWith(MockitoExtension.class)
class DeliveryTest {
  private static final Duration DELIVERY_TIME = Duration.ofMinutes(3);
  private static final long CUSTOMER_WAIT_TIME = 7L;

  @Mock
  private Order order;

  @Mock
  private LocalTime receivedAt;

  @Mock
  private LocalTime droneDepartedAt;

  @Mock
  private LocalTime deliveredAt;

  private Delivery delivery;

  @BeforeEach
  void createDelivery() {
    when(order.getReceivedAt()).thenReturn(receivedAt);
    when(order.getDeliveryTime()).thenReturn(DELIVERY_TIME);
    when(droneDepartedAt.plus(DELIVERY_TIME)).thenReturn(deliveredAt);
    when(receivedAt.until(deliveredAt, NANOS)).thenReturn(CUSTOMER_WAIT_TIME);
    delivery = new Delivery(order, droneDepartedAt);
  }

  @Test
  void getOrder() {
    assertThat(delivery.getOrder()).isEqualTo(order);
  }

  @Test
  void getDroneDepartedAt() {
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.of(droneDepartedAt));
  }

  @Test
  void getDroneDepartedAtNull() {
    delivery = new Delivery(order, null);
    assertThat(delivery.getDroneDepartedAt()).isEqualTo(Optional.empty());
  }

  @Test
  void getDeliveryTime() {
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofNanos(CUSTOMER_WAIT_TIME));
  }

  @Test
  void getDeliveryTimeNullDroneDepartedAt() {
    delivery = new Delivery(order, null);
    assertThat(delivery.getCustomerWaitTime()).isEqualTo(Duration.ofDays(1));
  }

  @Test
  void equalsContract() {
    EqualsVerifier.forClass(Delivery.class)
      .withIgnoredFields("droneDepartedAt", "customerWaitTime")
      .verify();
  }
}
