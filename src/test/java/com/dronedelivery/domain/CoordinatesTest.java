package com.dronedelivery.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;

import static com.dronedelivery.domain.Coordinates.DRONE_GROUND_SPEED_PER_BLOCK_IN_MINUTES;
import static com.dronedelivery.domain.Coordinates.Vector.Direction.NORTH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link Coordinates}.
 */
@ExtendWith(MockitoExtension.class)
class CoordinatesTest {
  private static final Integer NS_VECTOR_MAGNITUDE = 1;
  private static final Integer EW_VECTOR_MAGNITUDE = 2;
  private static final Integer VECTOR_MAGNITUDE = 3;
  private static final Coordinates.Vector.Direction VECTOR_DIRECTION = NORTH;

  @Mock
  private Coordinates.Vector nsVector;

  @Mock
  private Coordinates.Vector ewVector;

  private Coordinates coords;
  private Coordinates.Vector vector;

  @BeforeEach
  void createCoordsAndVector() {
    when(nsVector.getMagnitude()).thenReturn(NS_VECTOR_MAGNITUDE);
    when(ewVector.getMagnitude()).thenReturn(EW_VECTOR_MAGNITUDE);
    coords = new Coordinates(nsVector, ewVector);
    vector = new Coordinates.Vector(VECTOR_DIRECTION, VECTOR_MAGNITUDE);
  }

  @Test
  void getNsVector() {
    assertThat(coords.getNsVector()).isEqualTo(nsVector);
  }

  @Test
  void getEwVector() {
    assertThat(coords.getEwVector()).isEqualTo(ewVector);
  }

  @Test
  void getDeliveryTime() {
    assertThat(coords.getDeliveryTime()).isEqualTo(
      Duration.ofMinutes((NS_VECTOR_MAGNITUDE + EW_VECTOR_MAGNITUDE) * DRONE_GROUND_SPEED_PER_BLOCK_IN_MINUTES));
  }

  @Test
  void getRoundTripTime() {
    assertThat(coords.getRoundTripTime()).isEqualTo(coords.getDeliveryTime().multipliedBy(2));
  }

  @Test
  void getDirection() {
    assertThat(vector.getDir()).isEqualTo(VECTOR_DIRECTION);
  }

  @Test
  void getMagnitude() {
    assertThat(vector.getMagnitude()).isEqualTo(VECTOR_MAGNITUDE);
  }
}
