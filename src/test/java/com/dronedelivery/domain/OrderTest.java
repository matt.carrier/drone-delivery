package com.dronedelivery.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link Order}.
 */
@ExtendWith(MockitoExtension.class)
class OrderTest {
  private static final Duration DELIVERY_TIME = Duration.ofMinutes(30);

  @Mock
  private Coordinates coords;

  @Mock
  private LocalTime receivedAt;

  private Order order;

  @BeforeEach
  void createOrder() {
    order = new Order("id", coords, receivedAt);
  }

  @Test
  void getId() {
    assertThat(order.getId()).isEqualTo("id");
  }

  @Test
  void getCoords() {
    assertThat(order.getCoords()).isEqualTo(coords);
  }

  @Test
  void getReceivedAt() {
    assertThat(order.getReceivedAt()).isEqualTo(receivedAt);
  }

  @Test
  void getDeliveryTime() {
    when(coords.getDeliveryTime()).thenReturn(DELIVERY_TIME);
    assertThat(order.getDeliveryTime()).isEqualTo(DELIVERY_TIME);
  }

  @Test
  void getRoundTripTime() {
    when(coords.getRoundTripTime()).thenReturn(DELIVERY_TIME.multipliedBy(2));
    assertThat(order.getRoundTripTime()).isEqualTo(DELIVERY_TIME.multipliedBy(2));
  }

  @Test
  void equalsContract() {
    EqualsVerifier.forClass(Order.class)
      .withIgnoredFields("coords", "receivedAt")
      .verify();
  }
}
