package com.dronedelivery.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link RankedOrder}.
 */
@ExtendWith(MockitoExtension.class)
class RankOrderTest {
  private static final Integer RANK_1_2 = 0;
  private static final Integer RANK_3 = 1;

  @Mock
  private Order order;

  @Mock
  private Order order2;

  @Mock
  private Order order3;

  private RankedOrder rankedOrder;
  private RankedOrder rankedOrder2;
  private RankedOrder rankedOrder3;

  @BeforeEach
  void createRankedOrder() {
    rankedOrder = new RankedOrder(RANK_1_2, order);
    rankedOrder2 = new RankedOrder(RANK_1_2, order2);
    rankedOrder3 = new RankedOrder(RANK_3, order3);
  }

  @Test
  void getOrder() {
    assertThat(rankedOrder.getOrder()).isEqualTo(order);
  }

  @Test
  void compareTo() {
    assertThat(rankedOrder.compareTo(rankedOrder3)).isLessThan(0);
    assertThat(rankedOrder3.compareTo(rankedOrder)).isGreaterThan(0);
    assertThat(rankedOrder.compareTo(rankedOrder2)).isEqualTo(0);
  }

  @Test
  void equalsContract() {
    EqualsVerifier.forClass(RankedOrder.class)
      .withIgnoredFields("rank")
      .verify();
  }
}
