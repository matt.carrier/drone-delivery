package com.dronedelivery.domain;

import com.dronedelivery.score.Scores;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.dronedelivery.score.Scores.NPS;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link ScheduleResult}.
 */
@ExtendWith(MockitoExtension.class)
class ScheduleResultTest {
  private static final Scores scoringMethod = NPS;
  private static final int score = 8888;

  @Mock
  private List<Delivery> scheduledDeliveries;

  @Test
  void create() {
    final ScheduleResult scheduleResult = new ScheduleResult(scoringMethod, score, scheduledDeliveries);
    assertThat(scheduleResult.getScheduledDeliveries()).isEqualTo(scheduledDeliveries);
    assertThat(scheduleResult.getScoringMethod()).isEqualTo(scoringMethod);
    assertThat(scheduleResult.getScore()).isEqualTo(score);
  }
}
