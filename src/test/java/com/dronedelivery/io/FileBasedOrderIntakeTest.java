package com.dronedelivery.io;

import com.dronedelivery.Integration;
import com.dronedelivery.domain.Coordinates;
import com.dronedelivery.domain.Order;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.List;

import static com.dronedelivery.domain.Coordinates.Vector.Direction.EAST;
import static com.dronedelivery.domain.Coordinates.Vector.Direction.NORTH;
import static com.dronedelivery.domain.Coordinates.Vector.Direction.SOUTH;
import static com.dronedelivery.domain.Coordinates.Vector.Direction.WEST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link FileBasedOrderIntake}.
 */
@Integration
class FileBasedOrderIntakeTest {
  private final FileBasedOrderIntake intake = new FileBasedOrderIntake();

  @Test
  void valid() throws IOException {
    File file = new File(FileBasedOrderIntakeTest.class.getResource("/example_orders.in").getPath());
    List<Order> orders = intake.intake(file);

    assertThat(orders.size()).isEqualTo(4);
    assertOrder(orders.get(0), "WM001", LocalTime.of(5, 11, 50), 11, NORTH, 5, WEST);
    assertOrder(orders.get(1), "WM002", LocalTime.of(5, 11, 55), 3, SOUTH, 2, EAST);
    assertOrder(orders.get(2), "WM003", LocalTime.of(5, 31, 50), 7, NORTH, 50, EAST);
    assertOrder(orders.get(3), "WM004", LocalTime.of(6, 11, 50), 11, NORTH, 5, EAST);
  }

  @Test
  void invalidCoords() throws IOException {
    File file = new File(FileBasedOrderIntakeTest.class.getResource("/invalid_data/invalid_coords.in").getPath());
    List<Order> orders = intake.intake(file);

    assertThat(orders.size()).isEqualTo(3);
    assertOrder(orders.get(0), "WM001", LocalTime.of(5, 11, 50), 11, NORTH, 5, WEST);
    assertOrder(orders.get(1), "WM002", LocalTime.of(5, 11, 55), 3, SOUTH, 2, EAST);
    assertOrder(orders.get(2), "WM004", LocalTime.of(6, 11, 50), 11, NORTH, 5, EAST);
  }

  @Test
  void invalidOrder() throws IOException {
    File file = new File(FileBasedOrderIntakeTest.class.getResource("/invalid_data/invalid_order.in").getPath());
    List<Order> orders = intake.intake(file);

    assertThat(orders.size()).isEqualTo(3);
    assertOrder(orders.get(0), "WM001", LocalTime.of(5, 11, 50), 11, NORTH, 5, WEST);
    assertOrder(orders.get(1), "WM002", LocalTime.of(5, 11, 55), 3, SOUTH, 2, EAST);
    assertOrder(orders.get(2), "WM004", LocalTime.of(6, 11, 50), 11, NORTH, 5, EAST);
  }

  @Test
  void invalidOrderId() throws IOException {
    File file = new File(FileBasedOrderIntakeTest.class.getResource("/invalid_data/invalid_orderId.in").getPath());
    List<Order> orders = intake.intake(file);

    assertThat(orders.size()).isEqualTo(3);
    assertOrder(orders.get(0), "WM001", LocalTime.of(5, 11, 50), 11, NORTH, 5, WEST);
    assertOrder(orders.get(1), "WM002", LocalTime.of(5, 11, 55), 3, SOUTH, 2, EAST);
    assertOrder(orders.get(2), "WM004", LocalTime.of(6, 11, 50), 11, NORTH, 5, EAST);
  }

  @Test
  void invalidReceivedAt() throws IOException {
    File file = new File(FileBasedOrderIntakeTest.class.getResource("/invalid_data/invalid_receivedAt.in").getPath());
    List<Order> orders = intake.intake(file);

    assertThat(orders.size()).isEqualTo(3);
    assertOrder(orders.get(0), "WM001", LocalTime.of(5, 11, 50), 11, NORTH, 5, WEST);
    assertOrder(orders.get(1), "WM002", LocalTime.of(5, 11, 55), 3, SOUTH, 2, EAST);
    assertOrder(orders.get(2), "WM004", LocalTime.of(6, 11, 50), 11, NORTH, 5, EAST);
  }

  /**
   * Asserts an {@link Order}.
   *
   * @param order the order
   * @param orderId the order identifier
   * @param receivedAt the received at for the order
   * @param nsVectorMag the north/south vector magnitude
   * @param nsVectorDir the north/south vector direction
   * @param ewVectorMag the east/west vector magnitude
   * @param ewVectorDir the east/west vector direction
   */
  void assertOrder(Order order,
                   String orderId,
                   LocalTime receivedAt,
                   int nsVectorMag, Coordinates.Vector.Direction nsVectorDir,
                   int ewVectorMag, Coordinates.Vector.Direction ewVectorDir) {
    assertThat(order.getId()).isEqualTo(orderId);

    Coordinates coords = order.getCoords();
    assertVector(coords.getNsVector(), nsVectorMag, nsVectorDir);
    assertVector(coords.getEwVector(), ewVectorMag, ewVectorDir);

    assertThat(order.getReceivedAt()).isEqualTo(receivedAt);
  }

  /**
   * Asserts a vector.
   *
   * @param vector the vector
   * @param mag the magnitude
   * @param dir the direction
   */
  void assertVector(Coordinates.Vector vector, int mag, Coordinates.Vector.Direction dir) {
    assertThat(vector.getMagnitude()).isEqualTo(mag);
    assertThat(vector.getDir()).isEqualTo(dir);
  }
}
