package com.dronedelivery.io;

import com.dronedelivery.Integration;
import com.dronedelivery.domain.Delivery;
import com.dronedelivery.domain.Order;
import com.dronedelivery.domain.ScheduleResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dronedelivery.io.FileBasedOrderIntake.TIME_FORMATTER;
import static com.dronedelivery.score.Scores.NPS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link FileBasedDeliveryOutput}.
 */
@Integration
@ExtendWith(MockitoExtension.class)
class FileBasedDeliveryOutputTest {
  private static final String ORDER_ID = "orderId";
  private static final String ORDER2_ID = "order2Id";
  private static final int SCORE = 65;

  @Mock
  private ScheduleResult schedule;

  @Mock
  private Delivery delivery;

  @Mock
  private Order order;

  @Mock
  private Delivery delivery2;

  @Mock
  private Order order2;

  private List<Delivery> deliveries;
  private FileBasedDeliveryOutput output;
  private File file;

  @BeforeEach
  void reset() {
    deliveries = new ArrayList<>();
    output = new FileBasedDeliveryOutput();

    when(schedule.getScoringMethod()).thenReturn(NPS);
    when(schedule.getScheduledDeliveries()).thenReturn(deliveries);
    when(schedule.getScoringMethod()).thenReturn(NPS);
    when(schedule.getScore()).thenReturn(SCORE);
  }

  @AfterEach
  void deleteFile() {
    file.deleteOnExit();
  }

  /**
   * Mocks a delivery and adds it to the list of deliveries.
   *
   * @param delivery the delivery
   * @param order the order
   * @param orderId the id of the order
   * @param droneDepartedAt the drone departed at date for the delivery
   */
  void mockDelivery(Delivery delivery, Order order, String orderId, LocalTime droneDepartedAt) {
    when(order.getId()).thenReturn(orderId);
    when(delivery.getOrder()).thenReturn(order);
    mockDelivery(delivery, droneDepartedAt);
  }

  /**
   * Mocks a delivery and adds it to the list of deliveries.
   *
   * @param delivery the delivery
   * @param droneDepartedAt the drone departed at date for the delivery
   */
  void mockDelivery(Delivery delivery, LocalTime droneDepartedAt) {
    when(delivery.getDroneDepartedAt()).thenReturn(Optional.ofNullable(droneDepartedAt));
    deliveries.add(delivery);
  }

  @Test
  void score() throws IOException {
    file = output.output(schedule);
    assertThat(file).isNotNull();

    final List<String> lines;
    try (final FileReader fileReader = new FileReader(file);
         final BufferedReader reader = new BufferedReader(fileReader)) {
      lines = reader.lines().collect(Collectors.toList());
    }

    assertThat(lines.size()).isEqualTo(1);
    assertThat(lines.get(0)).isEqualTo(NPS.name() + " " + SCORE);
  }

  @Test
  void delivered() throws IOException {
    LocalTime deliveryDroneDepartedAt = LocalTime.NOON;
    LocalTime delivery2DroneDepartedAt = LocalTime.NOON.plusHours(1);
    mockDelivery(delivery, order, ORDER_ID, deliveryDroneDepartedAt);
    mockDelivery(delivery2, order2, ORDER2_ID, delivery2DroneDepartedAt);

    file = output.output(schedule);
    assertThat(file).isNotNull();

    final List<String> lines;
    try (final FileReader fileReader = new FileReader(file);
         final BufferedReader reader = new BufferedReader(fileReader)) {
      lines = reader.lines().collect(Collectors.toList());
    }

    assertThat(lines.size()).isEqualTo(3);
    assertThat(lines.get(0)).isEqualTo(ORDER_ID + " " + deliveryDroneDepartedAt.format(TIME_FORMATTER));
    assertThat(lines.get(1)).isEqualTo(ORDER2_ID + " " + delivery2DroneDepartedAt.format(TIME_FORMATTER));
    assertThat(lines.get(2)).isEqualTo(NPS.name() + " " + SCORE);
  }

  @Test
  void notDelivered() throws IOException {
    mockDelivery(delivery, null);

    file = output.output(schedule);
    assertThat(file).isNotNull();

    final List<String> lines;
    try (final FileReader fileReader = new FileReader(file);
         final BufferedReader reader = new BufferedReader(fileReader)) {
      lines = reader.lines().collect(Collectors.toList());
    }

    assertThat(lines.size()).isEqualTo(1);
    assertThat(lines.get(0)).isEqualTo(NPS.name() + " " + SCORE);
  }
}
