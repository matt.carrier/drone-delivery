package com.dronedelivery.io;

import com.dronedelivery.domain.Coordinates;
import com.dronedelivery.domain.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * {@link File} based {@link OrderIntake}.
 */
public class FileBasedOrderIntake implements OrderIntake<File> {
  private static final Logger log = LogManager.getLogger(FileBasedOrderIntake.class);

  private static final Pattern ID_PATTERN = Pattern.compile("^(?i)WM\\d+$");
  private static final Pattern COORDS_PATTERN = Pattern.compile("^(?i)([NS]\\d+)([EW]\\d+)$");

  public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

  private static final Map<String, Coordinates.Vector.Direction> DIRECTION_MAP =
    Stream.of(Coordinates.Vector.Direction.values()).collect(Collectors.toMap(
      d -> d.name().substring(0, 1),
      d -> d
    ));

  @Override
  public List<Order> intake(File file) throws IOException {
    try (FileReader fileReader = new FileReader(file);
         BufferedReader reader = new BufferedReader(fileReader)) {
      return reader.lines()
        .map(String::trim)
        .map(this::buildOrder)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(Collectors.toList());
    }
  }

  /**
   * Builds an {@link Order} from a file row.
   *
   * @param s the file row
   * @return the optional order
   */
  private Optional<Order> buildOrder(String s) {
    final String[] split = s.split(" ");
    if (3 != split.length) {
      log.warn(String.format("Row [%s] -- Invalid Order format", s));
      return Optional.empty();
    }

    final String id = split[0];
    if (!ID_PATTERN.matcher(id).matches()) {
      log.warn(String.format("Row [%s] -- Invalid Customer Id [%s]", s, id));
      return Optional.empty();
    }

    final String timeStr = split[2];
    final LocalTime receivedAt;
    try {
      receivedAt = LocalTime.parse(split[2], TIME_FORMATTER);
    } catch (DateTimeParseException e) {
      log.warn(String.format("Row [%s] -- Invalid Timestamp [%s]", s, timeStr));
      return Optional.empty();
    }

    return buildCoords(split[1], s).map(coords -> new Order(id, coords, receivedAt));
  }

  /**
   * Builds a delivery location from a file row segment.
   *
   * @param s the file row segment
   * @param row the file row
   * @return the delivery location
   */
  private Optional<Coordinates> buildCoords(String s, String row) {
    final Matcher matcher = COORDS_PATTERN.matcher(s);
    if (!matcher.matches()) {
      log.warn(String.format("Row [%s] -- Invalid coordinates [%s]", row, s));
      return Optional.empty();
    }

    return Optional.of(new Coordinates(buildCoordVector(matcher.group(1)), buildCoordVector(matcher.group(2))));
  }

  /**
   * Builds a delivery location's vector from a file row segment.
   *
   * @param s the file row segment
   * @return the delivery location's vector
   */
  private Coordinates.Vector buildCoordVector(String s) {
    return new Coordinates.Vector(buildCoordVectorDirection(s.substring(0, 1)), Integer.valueOf(s.substring(1)));
  }

  /**
   * Builds a delivery location's vector's direction from a file row segment.
   *
   * @param s the file row segment
   * @return the delivery location's vector's direction
   */
  private Coordinates.Vector.Direction buildCoordVectorDirection(String s) {
    return DIRECTION_MAP.get(s.toUpperCase());
  }
}
