package com.dronedelivery.io;

import com.dronedelivery.domain.Delivery;
import com.dronedelivery.domain.ScheduleResult;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.dronedelivery.io.FileBasedOrderIntake.TIME_FORMATTER;

/**
 * Outputs a delivery schedule to a file.
 */
public class FileBasedDeliveryOutput implements DeliveryOutput<File> {
  private final File directory;

  /**
   * Creates a new instance that will output a file to the system temp directory.
   */
  public FileBasedDeliveryOutput() {
    this(null);
  }

  /**
   * Creates a new instance that will output a file to the specified directory.
   *
   * @param directory the directory to output the file
   */
  public FileBasedDeliveryOutput(File directory) {
    this.directory = directory;
  }

  @Override
  public File output(ScheduleResult schedule) throws IOException {
    final File file = File.createTempFile("drone-deliveries", ".out", directory);

    try (FileWriter writer = new FileWriter(file)) {
      for (Delivery d : schedule.getScheduledDeliveries()) {
        // don't include orders that weren't delivered
        if (!d.getDroneDepartedAt().isPresent()) {
          break;
        }

        writer
          .write(String.format("%s %s\n", d.getOrder().getId(), d.getDroneDepartedAt().get().format(TIME_FORMATTER)));
      }

      writer.write(String.format("%s %d\n", schedule.getScoringMethod().name(), schedule.getScore()));
    }

    return file;
  }
}
