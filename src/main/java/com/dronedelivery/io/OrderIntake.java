package com.dronedelivery.io;

import com.dronedelivery.domain.Order;

import java.io.IOException;
import java.util.List;

/**
 * Intakes a known type into a {@link List} of {@link Order}s.
 */
public interface OrderIntake<T> {
  /**
   * Intakes on object into a {@link List} of {@link Order}s.
   *
   * @param obj the object to intake
   * @return the created orders
   * @throws IOException if there were any problems creating the orders
   */
  List<Order> intake(T obj) throws IOException;
}
