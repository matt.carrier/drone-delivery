package com.dronedelivery.io;

import com.dronedelivery.domain.ScheduleResult;

import java.io.IOException;

/**
 * Outputs a {@link ScheduleResult}.
 */
public interface DeliveryOutput<T> {
  /**
   * Outputs a delivery schedule.
   *
   * @param schedule the delivery schedule
   * @return the output
   * @throws IOException if there was an issue outputting the schedule
   */
  T output(ScheduleResult schedule) throws IOException;
}
