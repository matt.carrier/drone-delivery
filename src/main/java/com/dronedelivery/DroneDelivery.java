package com.dronedelivery;

import com.dronedelivery.domain.Order;
import com.dronedelivery.domain.ScheduleResult;
import com.dronedelivery.io.FileBasedDeliveryOutput;
import com.dronedelivery.io.FileBasedOrderIntake;
import com.dronedelivery.score.Score;
import com.dronedelivery.score.Scores;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.io.File;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.Callable;

import static com.dronedelivery.score.Scores.NPS;
import static picocli.CommandLine.Help.Visibility.ALWAYS;

/**
 * Commandline interface for drone delivery.
 */
@CommandLine.Command(
  description = "Builds a drone delivery list based on maximization of the scoring method.",
  name = "drone_delivery"
)
public class DroneDelivery implements Callable<File> {
  private static final Logger log = LogManager.getLogger(DroneDelivery.class);

  public static final LocalTime DRONE_LAUNCH_TIME = LocalTime.of(6,0);
  public static final LocalTime DRONE_RETURN_TIME = LocalTime.of(22,0);

  @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display this help message")
  boolean usageHelpRequested;

  @CommandLine.Parameters(
    index = "0",
    description = "The file containing the orders to deliver."
  )
  File file;

  @CommandLine.Option(
    names = {"-d", "--dir"},
    description = "The output directory."
  )
  private File dir;

  @CommandLine.Option(
    names = {"-s", "--score"},
    description = "Scoring method for ranking: {${COMPLETION-CANDIDATES}}",
    showDefaultValue = ALWAYS
  )
  private Scores scoringMethod = NPS;

  public static void main(String[] args) {
    CommandLine.call(new DroneDelivery(), args);
  }

  @Override
  public File call() throws Exception {
    Score score = scoringMethod.create();
    List<Order> orders = new FileBasedOrderIntake().intake(file);
    ScheduleResult schedule = score.schedule(orders, DRONE_LAUNCH_TIME, DRONE_RETURN_TIME);
    File outputFile = new FileBasedDeliveryOutput(dir).output(schedule);
    log.info(String.format("Scheduled Deliveries: [%s]", outputFile.getAbsolutePath()));
    return outputFile;
  }
}
