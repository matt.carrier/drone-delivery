package com.dronedelivery.score;

/**
 * All available scoring methods.
 */
public enum Scores {
  NPS(NetPromoterScore.class); // Net Promoter Score

  private final Class<? extends Score> clazz;

  /**
   * Creates an instance.
   *
   * @param clazz the scoring method class
   */
  Scores(Class<? extends Score> clazz) {
    this.clazz = clazz;
  }

  /**
   * Creates a new instance of the scoring method.
   *
   * @return the new instance
   */
  public Score create() {
    try {
      return clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
