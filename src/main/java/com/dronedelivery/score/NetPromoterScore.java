package com.dronedelivery.score;

import com.dronedelivery.domain.Delivery;
import com.dronedelivery.domain.Order;
import com.dronedelivery.domain.RankedOrder;
import com.dronedelivery.domain.ScheduleResult;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.dronedelivery.score.Scores.NPS;
import static java.time.LocalTime.MIDNIGHT;
import static java.time.LocalTime.NOON;

/**
 * A Net Promoter Score defines customer satisfaction over a collection of {@link Order} objects.
 * <p>
 * Customers fit into one of three buckets:
 * <ul>
 * <li>promoter (happy enough to persuade others to NOT use the service)</li>
 * <li>neutral (happy enough to NOT persuade others to NOT use the service)</li>
 * <li>detractor (disatisfied enough to persuade others to NOT use the service)</li>
 * </ul>
 * </p>
 */
public class NetPromoterScore implements Score {
  private static final Duration NEUTRAL_BOUND = Duration.ofHours(4).minusSeconds(1);

  @Override
  public ScheduleResult schedule(Collection<Order> orders, LocalTime start, LocalTime end) {
    final List<Delivery> scheduled = new ArrayList<>();

    //------------------------------------------------------------------------------------------------------------------
    //  Initially sort the orders by distance
    //------------------------------------------------------------------------------------------------------------------

    final List<Order> sorted = orders.stream()
      .map(order -> {
        //------------------------------------------------------------------------------------------------------------------
        // Known Detractors (no ability to turn these customers into neutrals or promoters
        //
        //    Automatic lowest priority
        //
        //  1. If the customer is simply too far away to be delivered in 4 hours
        //  2. If the order was received on or before 2am
        //  3. If the drone can't get to the customer and return before 10pm
        //------------------------------------------------------------------------------------------------------------------

        if (0 < order.getDeliveryTime().compareTo(NEUTRAL_BOUND) ||
          0 <= MIDNIGHT.plusHours(2).compareTo(order.getReceivedAt()) ||
          order.getReceivedAt().isAfter(NOON.plusHours(10).minus(order.getRoundTripTime()))) {
          return new RankedOrder(Integer.MAX_VALUE, order);
        }

        //------------------------------------------------------------------------------------------------------------------
        // Rank by trip time asc
        //------------------------------------------------------------------------------------------------------------------

        return new RankedOrder((int) order.getRoundTripTime().getSeconds(), order);
      })
      .sorted()
      .map(RankedOrder::getOrder)
      .collect(Collectors.toList());

    //------------------------------------------------------------------------------------------------------------------
    //  Iterate through the sorted list to find the best item to deliver for each drone delivery
    //------------------------------------------------------------------------------------------------------------------

    List<Order> pending = new ArrayList<>();
    LocalTime nextDepartureTime = start;
    for (Order order : sorted) {

      // TODO: enhance the pending orders algorithm to balance waiting for delayed
      //       items vs delivering the next shortest-trip eligible item

      nextDepartureTime = checkPendingOrders(pending, scheduled, nextDepartureTime, end);

      if (nextDepartureTime.isBefore(order.getReceivedAt())) {
        pending.add(order);
        continue;
      }

      nextDepartureTime = scheduleOrder(order, scheduled, nextDepartureTime, end);
    }

    //------------------------------------------------------------------------------------------------------------------
    //  if we have got to this point and still have pending orders it means that we need to delay the next delivery
    //  until the first pending order's received at time
    //------------------------------------------------------------------------------------------------------------------

    while (!pending.isEmpty()) {
      checkPendingOrders(pending, scheduled, pending.get(0).getReceivedAt(), end);
    }

    System.out.println(scheduled.size());
    return new ScheduleResult(NPS, calculate(scheduled), scheduled);
  }

  /**
   * Checks existing pending orders to see if they can be delivered.
   *
   * @param pending the pending order to check
   * @param deliveries the current delivery list
   * @param nextDepartureTime the next departure time
   * @param end the end of the drone delivery window
   * @return the new next departure date
   */
  private LocalTime checkPendingOrders(List<Order> pending, List<Delivery> deliveries, LocalTime nextDepartureTime,
                                       LocalTime end) {
    LocalTime localNextDepartureTime = nextDepartureTime;
    Iterator<Order> i = pending.iterator();
    while (i.hasNext()) {
      Order order = i.next();
      if (!localNextDepartureTime.isBefore(order.getReceivedAt())) {
        localNextDepartureTime = scheduleOrder(order, deliveries, localNextDepartureTime, end);
        i.remove();
      }
    }

    return localNextDepartureTime;
  }

  /**
   * Schedules the given order for delivery.
   *
   * @param order the order to add to the schedule
   * @param deliveries the current delivery list
   * @param nextDepartureTime the next departure time
   * @param end the end of the drone delivery window
   * @return the new next deparature date
   */
  private LocalTime scheduleOrder(Order order, List<Delivery> deliveries, LocalTime nextDepartureTime,
                                  LocalTime end) {
    final LocalTime localNextDepartureTime;
    Duration totalTripTime = order.getRoundTripTime();
    if (end.isBefore(nextDepartureTime.plus(totalTripTime))) {
      // we have reached the end of the drone delivery window for the day
      deliveries.add(new Delivery(order, null));
      localNextDepartureTime = LocalTime.MAX;
    } else {
      deliveries.add(new Delivery(order, nextDepartureTime));
      localNextDepartureTime = nextDepartureTime.plus(totalTripTime);
    }

    return localNextDepartureTime;
  }


  /**
   * Calculates the NPS score for the given deliveries.
   *
   * @param deliveries the deliveries
   * @return the NPS score
   */
  private int calculate(List<Delivery> deliveries) {
    if (deliveries.isEmpty()) {
      return -1;
    }

    int detractors = 0;
    int promoters = 0;
    for (Delivery delivery : deliveries) {
      if (Duration.ofHours(2).getSeconds() > delivery.getCustomerWaitTime().getSeconds()) {
        promoters++;
      } else if (Duration.ofHours(4).getSeconds() <= delivery.getCustomerWaitTime().getSeconds()) {
        detractors++;
      }
    }

    final int totalDeliveries = deliveries.size();
    return (int) (percentage(promoters, totalDeliveries) - percentage(detractors, totalDeliveries));
  }

  /**
   * Calculates a percentage.
   *
   * @param numerator the numerator
   * @param denominator the denominator
   * @return the percentage
   */
  private double percentage(int numerator, double denominator) {
    return numerator / denominator * 100;
  }
}
