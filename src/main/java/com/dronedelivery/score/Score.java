package com.dronedelivery.score;

import com.dronedelivery.domain.Order;
import com.dronedelivery.domain.ScheduleResult;

import java.time.LocalTime;
import java.util.Collection;

/**
 * Represents a scoring method for order delivery.
 */
public interface Score {
  /**
   * Schedules a collection of orders that maximize the score..
   *
   * @param orders the orders to schedule
   * @param start when the delivers can start
   * @param end when the drone needs to be back
   * @return the delivery schedule
   */
  ScheduleResult schedule(Collection<Order> orders, LocalTime start, LocalTime end);
}
