package com.dronedelivery.domain;

import java.util.Objects;

/**
 * Provides the ability to rank an {@link Order}.
 */
public final class RankedOrder implements Comparable<RankedOrder> {
  private final int rank;
  private final Order order;

  /**
   * Creates a new instance.
   *
   * @param rank the rank, the higher the number the sooner it should be delivered
   * @param order the order associated to the ranking
   */
  public RankedOrder(int rank, Order order) {
    this.rank = rank;
    this.order = order;
  }

  @Override
  public int compareTo(RankedOrder o) {
    return Integer.compare(rank, o.rank);
  }

  /**
   * Gets the {@link Order}.
   *
   * @return the order
   */
  public Order getOrder() {
    return order;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof RankedOrder)) { return false; }
    RankedOrder rankedOrder = (RankedOrder) o;
    return Objects.equals(order, rankedOrder.order);
  }

  @Override
  public int hashCode() {
    return Objects.hash(order);
  }
}
