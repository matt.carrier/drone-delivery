package com.dronedelivery.domain;

import com.dronedelivery.score.Scores;

import java.util.List;

/**
 * A delivery schedule with corresponding score.
 */
public class ScheduleResult {
  private final Scores scoringMethod;
  private final int score;
  private final List<Delivery> scheduledDeliveries;

  /**
   * Creates an instance.
   *
   * @param scoringMethod the scoring method used to generate this schedule
   * @param score the score for this schedule
   * @param scheduledDeliveries the scheduled deliveries
   */
  public ScheduleResult(Scores scoringMethod, int score, List<Delivery> scheduledDeliveries) {
    this.scoringMethod = scoringMethod;
    this.score = score;
    this.scheduledDeliveries = scheduledDeliveries;
  }

  /**
   * Gets the scoring method.
   *
   * @return the scoring method
   */
  public Scores getScoringMethod() {
    return scoringMethod;
  }

  /**
   * Gets the score.
   *
   * @return the score
   */
  public int getScore() {
    return score;
  }

  /**
   * Gets the scheduled deliveries.
   *
   * @return the scheduled deliveries
   */
  public List<Delivery> getScheduledDeliveries() {
    return scheduledDeliveries;
  }
}
