package com.dronedelivery.domain;

import java.time.Duration;

/**
 * Coordinates for a customer's location.
 */
public final class Coordinates {
  static final int DRONE_GROUND_SPEED_PER_BLOCK_IN_MINUTES = 1;

  private final Vector nsVector;
  private final Vector ewVector;
  private final Duration deliveryTime;
  private final Duration roundTripTime;

  /**
   * Creates an instance
   *
   * @param nsVector the north/south vector
   * @param ewVector the east/west vector
   */
  public Coordinates(Vector nsVector, Vector ewVector) {
    this.nsVector = nsVector;
    this.ewVector = ewVector;
    deliveryTime =
      Duration.ofMinutes((nsVector.getMagnitude() + ewVector.getMagnitude()) * DRONE_GROUND_SPEED_PER_BLOCK_IN_MINUTES);
    roundTripTime = deliveryTime.multipliedBy(2);
  }

  /**
   * Gets the north/south vector.
   *
   * @return the north/south vector
   */
  public Vector getNsVector() {
    return nsVector;
  }

  /**
   * Gets the east/west vector.
   *
   * @return the east/west vector
   */
  public Vector getEwVector() {
    return ewVector;
  }

  /**
   * Gets the delivery time.
   *
   * @return the delivery time
   */
  public Duration getDeliveryTime() {
    return deliveryTime;
  }

  /**
   * Gets the round trip time time.
   *
   * @return the round trip time
   */
  public Duration getRoundTripTime() {
    return roundTripTime;
  }

  /**
   * A tuple of direction and magnitude.
   */
  public static class Vector {
    /**
     * Cardinal directions.
     */
    public enum Direction {
      NORTH,
      SOUTH,
      EAST,
      WEST
    }

    private final Direction dir;
    private final int magnitude;

    /**
     * Creates an instance.
     *
     * @param dir the direction
     * @param magnitude the magnitude
     */
    public Vector(Direction dir, int magnitude) {
      this.dir = dir;
      this.magnitude = magnitude;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    public Direction getDir() {
      return dir;
    }

    /**
     * Gets the magnitude.
     *
     * @return the magnitude
     */
    public int getMagnitude() {
      return magnitude;
    }
  }
}
