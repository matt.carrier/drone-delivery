package com.dronedelivery.domain;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Objects;

/**
 * An order to be delivered.
 */
public final class Order {
  private final String id;
  private final Coordinates coords;
  private final LocalTime receivedAt;

  /**
   * Creates an instance.
   *
   * @param id the id
   * @param coords the delivery coordinates
   * @param receivedAt when the order was received
   */
  public Order(String id, Coordinates coords, LocalTime receivedAt) {
    this.id = id;
    this.coords = coords;
    this.receivedAt = receivedAt;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Gets the delivery coordinates.
   *
   * @return the delivery coordinates
   */
  public Coordinates getCoords() {
    return coords;
  }

  /**
   * Gets the delivery time.
   *
   * @return the delivery time
   */
  public Duration getDeliveryTime() {
    return coords.getDeliveryTime();
  }

  /**
   * Gets the round trip time.
   *
   * @return the round trip time
   */
  public Duration getRoundTripTime() {
    return coords.getRoundTripTime();
  }

  /**
   * Gets the order received at.
   *
   * @return the order received at
   */
  public LocalTime getReceivedAt() {
    return receivedAt;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof Order)) { return false; }
    Order order = (Order) o;
    return Objects.equals(id, order.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
