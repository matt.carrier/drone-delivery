package com.dronedelivery.domain;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Optional;

/**
 * A delivery.
 */
public final class Delivery {
  private final Order order;
  private final LocalTime droneDepartedAt;
  private final Duration customerWaitTime;

  /**
   * Creates an instance.
   *
   * @param order the order
   * @param droneDepartedAt the time the drone departed with the order
   */
  public Delivery(Order order, LocalTime droneDepartedAt) {
    this.order = order;
    this.droneDepartedAt = droneDepartedAt;
    customerWaitTime = null == droneDepartedAt ? Duration.ofDays(1) : Duration
      .between(order.getReceivedAt(), droneDepartedAt.plus(order.getDeliveryTime()));
  }

  /**
   * Gets the order.
   *
   * @return the order
   */
  public Order getOrder() {
    return order;
  }

  /**
   * Gets the optional drone departed at.
   *
   * @return the optional drone departed at
   */
  public Optional<LocalTime> getDroneDepartedAt() {
    return Optional.ofNullable(droneDepartedAt);
  }

  /**
   * Gets the customer wait time.
   *
   * @return the customer wait time (will be integer max for undelivered items)
   */
  public Duration getCustomerWaitTime() {
    return customerWaitTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof Delivery)) { return false; }
    Delivery delivery = (Delivery) o;
    return Objects.equals(order, delivery.order);
  }

  @Override
  public int hashCode() {
    return Objects.hash(order);
  }
}
