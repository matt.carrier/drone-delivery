# Drone Delivery Challenge

[Challenge Description](doc/challenge.md)

## Givens

Part of this challenge is that the givens are quite limited. Let's list out what we know for sure:

### General

* Net promoter score (NPS) is the number 1 priority

### Town and Drone

* The town is organized in a perfect grid
* The warehouse and drone-launch facility are at the center of the town
* All deliveries originate at the warehouse
* All deliveries are carried by a drone to a customer location
* The town owns one drone
* The drone is only allowed to operate from 6 a.m. until 10 p.m
* The drone's "ground speed" is exactly one horizontal or vertical grid block per minute

### Input

* Input will be a file
* Each Row will be a customer order
* A customer order is comprised of an order identifier, customer's location, and order timestamp
* Order identifier will be of the format `WM####`
* The customer coordinate will have a North/South direction indicator (N or S) and a East/West indicator (E or W) -- for
    example: `N5E10`
* The timestamp will have the format: `HH:MM:SS`
* the customer orders will be sorted by order timestamp

#### Example Input File Rows

    WM001 N11W5 05:11:50
    WM002 S3E2 05:11:55
    WM003 N7E50 05:31:50
    WM004 N11E5 06:11:50
    ...

### Output

* The program should produce a file that contains one output line for each customer order
* Each line should indicate the order number and the drone's departure time
* The last line in the output file should contain the estimated NPS score

## Assumptions

In order to deliver a solution we're going to have to piece together these givens using some assumption glue. The
following assumptions have been made in order to deliver the 'best of our ability' solution.

**In a real-life situation forming assumptions and acting immediately should be a big no-no. In an agile environment
working directly with the PO to refine the givens and remove the need for as many assumptions as possible would be the
appropriate COA. Of course removing all assumptions may not be possible but it is important to arrive at these
assumptions in a group setting. The consensus of the whole will always be better or equal to the assumption from one.**

### Town and Drone

* The drone can only travel horizontal or vertical at any one time (it cannot travel diagonally across grid blocks)
* We are in a fictional land where weather, construction, drone malfunction, loading/unloading time, customer delivery
    verification, etc. does not affect the drone speed
* The drone can handle only one package at a time and has no package weight restriction

### Input/Output

* Somehow we have a list of orders for the current day before the drone starts
* Product is going to want to change to real-time order processing in the near future from batches per day
* Product is going to want to move away from file IO to something else sooner rather than later
* Product is going to want to try different scoring algorithms to maximize customer satisfaction

## Build

The drone-delivery build will output java executables and test reports in the following locations:

* The executable jar: `build/libs/drone-delivery-all.jar`
* Test reports: `build/reports/tests`

There are two options for building drone-delivery:

### Local Build

Building drone-delivery locally requires you to install Java.

#### Prerequisites

drone-delivery utilizes [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
features like streams and optionals so at the very least JDK 8 needs to be installed.

#### Command

drone-delivery utilizes [Gradle](https://gradle.org) to compile, assemble the executable, and run the tests. The wrapper
is utilized so no manual installation of gradle is required, the wrapper will handle all of that for you.

`./gradlew clean build` will compile the source, build the executable jar, and run the tests.

### Docker

If you already have [Docker](https://www.docker.com/) installed then you can just utilize docker to build
drone-delivery.

#### Command

`docker run --rm -v $(pwd):$(pwd) -w $(pwd) gradle:5.4.1-jdk8 gradle clean build`

## Execution

drone-delivery is packaged as a java executable, and there are two options for executing drone-delivery:

### Local Execution

Please see the [Local Build](#local-build) section for the prerequisites.

#### Example Command

Running the example orders while in the repository directory:

`java -jar build/libs/drone-delivery-all.jar src/test/resources/example_orders.in`

### Docker

If you already have [Docker](https://www.docker.com/) installed then you can just utilize docker to execute
drone-delivery.

#### Command

Make sure that you pass in the `-d` option as a relative directory to the repo or mount another directory into the
container and use that as your output directory.

Running the example orders while in the repository directory:

```
docker run --rm -v $(pwd):$(pwd) -w $(pwd) openjdk:8-jre java -jar build/libs/drone-delivery-all.jar -d $(pwd) src/test/resources/example_orders.in
```

### Usage Help

Just pass in `-h` for usage information.

```
$ docker run --rm -v $(pwd):$(pwd) -w $(pwd) openjdk:8-jre java -jar build/libs/drone-delivery-all.jar -h
Usage: drone_delivery [-h] [-d=<dir>] [-s=<scoringMethod>] <file>
Builds a drone delivery list based on maximization of the scoring method.
      <file>        The file containing the orders to deliver.
  -d, --dir=<dir>   The output directory.
  -h, --help        display this help message
  -s, --score=<scoringMethod>
                    Scoring method for ranking: {NPS}
                      Default: NPS
```
